Easy OAuth using DaliCore
=========================

This project contains the code that is being used in my blog about using
[DaliCore](http://java.net/projects/dalicore) to implement an OAuth provider and consumer.

The article itself is split in two parts:

 - [the service provider](http://tiainen.sertik.net/2012/01/easy-oauth-using-dalicore-and-glassfish.html)
 - [the service consumer](http://tiainen.sertik.net/2012/01/easy-oauth-using-dalicore-and-glassfish_31.html)
