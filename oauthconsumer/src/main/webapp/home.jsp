<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>dalicore-oauth service provider</title>
	</head>
	<body>
		<h1>Connected!</h1>

		<p>You successfully connected with the external network ${it.network}.</p>

		Access Token key: ${it.token}<br/>
		Access Token secret: ${it.secret}
	</body>
</html>
