package com.mycompany.oauthconsumer;

import com.lodgon.dali.core.social.DaliCoreExternalNetwork;
import com.lodgon.dali.core.social.entity.ExternalToken;
import com.sun.jersey.api.view.Viewable;
import java.net.URI;
import java.net.URISyntaxException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

@Path("home")
public class HomeHandler {
	private static final String PROVIDER_URL = "http://localhost:8080/oauthprovider/rest";
	private static final String CONSUMER_KEY = "b71519bcbda2fe74";
	private static final String CONSUMER_SECRET = "fa65941c9ccdff6f";
	private static final String CONSUMER_CALLBACK = "http://localhost:8080/oauthconsumer/rest/home/callback";

	@Context
	HttpServletRequest request;

	@GET
	public Response home() throws URISyntaxException {
		ExternalToken accessToken = (ExternalToken) request.getSession().getAttribute("accessToken");
		if (accessToken == null) {
			DaliCoreExternalNetwork externalNetwork = new DaliCoreExternalNetwork(PROVIDER_URL,
							CONSUMER_KEY, CONSUMER_SECRET);
			return externalNetwork.connect(CONSUMER_CALLBACK);
		} else {
			return Response.ok(new Viewable("/home.jsp", accessToken)).build();
		}
	}

	@GET
	@Path("callback")
	public Response callback(@QueryParam("oauth_token") String requestToken,
					@QueryParam("oauth_verifier") String verifier) throws URISyntaxException {
		DaliCoreExternalNetwork externalNetwork = new DaliCoreExternalNetwork(PROVIDER_URL,
						CONSUMER_KEY, CONSUMER_SECRET);
		ExternalToken accessToken = externalNetwork.callback(requestToken, verifier);
		request.getSession().setAttribute("accessToken", accessToken);
		return Response.seeOther(new URI("/home")).build();
	}
}
