package com.mycompany.oauthprovider;

import com.lodgon.dali.core.ejb.DaliCoreException;
import com.lodgon.dali.core.ejb.UserBean;
import com.lodgon.dali.core.entity.User;
import com.lodgon.dali.core.oauth.ejb.OAuthBean;
import com.lodgon.dali.core.oauth.entity.DaliServiceConsumer;
import com.lodgon.dali.core.oauth.entity.DaliToken;
import com.lodgon.dali.core.oauth.entity.OAuthUser;
import com.sun.jersey.api.view.Viewable;
import com.sun.jersey.oauth.signature.OAuthParameters;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import javax.annotation.ManagedBean;
import javax.inject.Inject;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

@Path("oauth")
@ManagedBean
public class OAuthHandler {
	@Inject
	OAuthBean oauthBean;
	@Inject
	UserBean userBean;

	@GET
	@Path("authenticate")
	public Viewable authenticate(@QueryParam("oauth_token") String oauthToken) {
		return new Viewable("/login.jsp", new Model(oauthToken));
	}

	@POST
	@Path("login")
	public Viewable login(@FormParam("oauth_token") String oauthToken,
					@FormParam("username") String userName,
					@FormParam("password") String password) throws DaliCoreException {
		User user = userBean.validateScreenNameAndPassword(userName, password);
		if (user == null) {
			user = new OAuthUser();
			user.setScreenName(userName);
			user = userBean.create(user);
			userBean.setPassword(user.getId(), password);
		}

		DaliToken requestToken = oauthBean.findDaliToken(oauthToken, DaliToken.Type.OAUTH_REQUEST_TOKEN);
		return new Viewable("/authorize.jsp", new Model(oauthToken, user, requestToken.getDaliServiceConsumer()));
	}

	@POST
	@Path("authorize")
	public Response authorize(@FormParam("oauth_token") String oauthToken,
					@FormParam("userUid") String userUid,
					@DefaultValue("no") @FormParam("authorize") String authorize) throws DaliCoreException, URISyntaxException, UnsupportedEncodingException {
		DaliToken requestToken = oauthBean.findDaliToken(oauthToken, DaliToken.Type.OAUTH_REQUEST_TOKEN);

		if ("yes".equals(authorize)) {
			User user = userBean.getByUid(userUid);
			if (user != null) {
				String verifier = oauthBean.authorizeServiceConsumer(user.getId(), oauthToken, true);
				if ("oob".equals(requestToken.getCallback())) {
					return Response.ok(verifier).build();
				} else {
					return Response.seeOther(new URI(requestToken.getCallback() + "?" + OAuthParameters.TOKEN + "=" + URLEncoder.encode(oauthToken, "UTF-8") + "&" + OAuthParameters.VERIFIER + "=" + URLEncoder.encode(verifier, "UTF-8"))).build();
				}
			} else {
				return Response.ok(new Viewable("/login.jsp", new Model(oauthToken))).build();
			}
		} else {
			return Response.seeOther(new URI(requestToken.getCallback())).build();
		}
	}

	public static class Model {
		private String message;
		private String oauthToken;
		private DaliServiceConsumer serviceConsumer;
		private User user;

		public Model(String oauthToken) {
			this.oauthToken = oauthToken;
		}

		public Model(String oauthToken, String message) {
			this.oauthToken = oauthToken;
			this.message = message;
		}

		public Model(String oauthToken, User user, DaliServiceConsumer serviceConsumer) {
			this.oauthToken = oauthToken;
			this.user = user;
			this.serviceConsumer = serviceConsumer;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public String getOauthToken() {
			return oauthToken;
		}

		public void setOauthToken(String oauthToken) {
			this.oauthToken = oauthToken;
		}

		public DaliServiceConsumer getServiceConsumer() {
			return serviceConsumer;
		}

		public void setServiceConsumer(DaliServiceConsumer serviceConsumer) {
			this.serviceConsumer = serviceConsumer;
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}
	}
}
