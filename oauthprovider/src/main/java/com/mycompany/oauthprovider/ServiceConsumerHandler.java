package com.mycompany.oauthprovider;

import com.lodgon.dali.core.oauth.ejb.OAuthBean;
import com.lodgon.dali.core.oauth.entity.DaliServiceConsumer;
import com.lodgon.dali.core.oauth.util.StringUtil;
import javax.annotation.ManagedBean;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

@Path("serviceconsumer")
@ManagedBean
public class ServiceConsumerHandler {
	@Inject
	OAuthBean oauthBean;

	@GET
	@Produces("text/html")
	public String create(@QueryParam("name") String name) {
		if (name != null && ! name.trim().isEmpty()) {
			DaliServiceConsumer daliServiceConsumer = new DaliServiceConsumer();
			daliServiceConsumer.setName(name);
			daliServiceConsumer.setConsumerKey(StringUtil.getSecureRandomString(16));
			daliServiceConsumer.setConsumerSecret(StringUtil.getSecureRandomString(16));
			daliServiceConsumer.setStatus(DaliServiceConsumer.Status.ACTIVE);
			oauthBean.createDaliServiceConsumer(daliServiceConsumer);

			return "consumer key: <code>" + daliServiceConsumer.getKey() + "</code><br/>consumer secret: <code>" + daliServiceConsumer.getSecret() + "</code>";
		}

		return "No name provided. Add ?name=service_consumer_name at the end of the URL.";
	}
}
