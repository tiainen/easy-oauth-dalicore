<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>dalicore-oauth service provider</title>
	</head>
	<body>
		<h1>Authorize</h1>

		<h4>Do you want to allow the service consumer <b>${it.serviceConsumer.name}</b> to access your data:</h4>

		<form method="POST" action="${pageContext.request.contextPath}/rest/oauth/authorize">
			<input type="hidden" name="oauth_token" value="${it.oauthToken}"/>
			<input type="hidden" name="userUid" value="${it.user.uid}"/>
			<input type="submit" name="authorize" value="yes"/>
			<input type="submit" name="authorize" value="no"/>
		</form>
	</body>
</html>
