<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>dalicore-oauth service provider</title>
	</head>
	<body>
		<h1>Authenticate</h1>

		<form method="POST" action="${pageContext.request.contextPath}/rest/oauth/login">
			<input type="hidden" name="oauth_token" value="${it.oauthToken}"/>
			username: <input type="text" name="username"/><br/>
			password: <input type="password" name="password"/><br/>
			<input type="submit" value="login"/>
		</form>
	</body>
</html>
